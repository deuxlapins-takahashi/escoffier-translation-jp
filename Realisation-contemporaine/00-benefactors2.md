---
title: エスコフィエ『料理の手引き』全注解
author:
- 五 島　学（責任編集・訳・注釈）
- 河井 健司（訳・注釈）
- 春野 裕征（訳）
- 山 本　学（訳）
- 高 橋　昇（校正）
pandoc-latex-environment:
    main: [main]
    recette: [recette]
    frsubenv: [frsubenv]
    frsecenv: [frsecenv]
    frsecbenv: [frsecbenv]
    frchapenv: [frchapenv]
...

 





## Facebookでのチャット会議参加者の皆様 {#participants}

\thispagestyle{empty}


\small 

「エスコフィエ『料理の手引き』全注解」プロジェクトの宣伝文作成のためのチャット会議に参加いただき、氏名の掲載をご承諾くださった方々は以下のとおりです（お申し出順、敬称略）。

\normalsize
\vspace{1\zw}

* [河井健司(Un de ces jours)](http://www.undecesjours.com/)

* [善塔一幸(La Maison Courtine)](http://www.courtine.jp/)

* 春野 裕征

* 山下 拓也

* 石 幡　乾(元エジンバラ総領事館)

* 高 橋　昇(レストラン ドゥ ラパン)

* 山 本　学(レチュード)


\vfill

\small


\normalsize
